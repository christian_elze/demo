package com.scorpiontech.example.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.scorpiontech.example.entities.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

  public List<Customer> findByFirstName(String firstName);
  public List<Customer> findByLastName(String lastName);

}